<?php
  session_start();
  $text = rand(1000, 9999);
  $_SESSION['captcha'] = $text;
  $fontSize = 30;
  $width = 120;
  $height = 40;
  
  $image = imagecreate($width, $height);
  imagecolorallocate($image, 255, 255, 255);
  $textColor = imagecolorallocate($image, 0, 0, 0);
  
  for ($i = 0; $i <= 30; $i++) {
    $x1 = rand(1, 100);
    $y1 = rand(1, 100);
    $x2 = rand(1, 100);
    $y2 = rand(1, 100);
    imageline($image, $x1, $y1, $x2, $y2, $text);
  }
  
  imagettftext($image, $fontSize, 0, 15, 30, $textColor, __DIR__ . '/fonts/OpenSans.ttf', $text);
  
  header('Content-type: image/png');
  imagepng($image);



