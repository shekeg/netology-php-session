<?php
  session_start();
  $testResult = $_SESSION['testResult'];
  $userName = (!empty($_SESSION['user']['login'])) ? $_SESSION['user']['login'] : "Не задано имя";
  $testName = (!empty($testResult['testName'])) ? "Пройден тест: {$testResult['testName']}" : "Не удалось получить имя теста";
  $totalCorrect = $testResult['totalCorrect'];
  $totalCorrect = (!empty($totalCorrect) || $totalCorrect === 0) ? "По результатам теста получена оценка: {$totalCorrect}" 
    : 'Не удалось получить оценку';

  $cert = imagecreatefrompng(__DIR__ . '/img/cert.png');
  $blue = imagecolorallocate($cert, 0, 173, 238);
  $black = imagecolorallocate($cert, 80, 97, 115);
  $fontFile = __DIR__ . '/fonts/OpenSans.ttf';
  if (!file_exists($fontFile)) {
    echo 'Файл со щрифтом не найден!';
    exit;
  }
  
  function pasteTextCenter($img, $y, $fontSize, $fontFile , $color, $text) {
    $bbox = imagettfbbox($fontSize, 0, $fontFile, $text);
    $left = (imagesx($img) / 2) - (($bbox[2] - $bbox[0]) / 2);
    imagettftext($img, $fontSize, 0, $left, $y, $color, $fontFile, $text);
  }

  pasteTextCenter($cert, 1230, 130, $fontFile, $blue, $userName);
  pasteTextCenter($cert, 1430, 70, $fontFile, $black, $testName);
  pasteTextCenter($cert, 1650, 70, $fontFile, $black, $totalCorrect);

  header('Content-type: image/png');
  imagepng($cert);
  imagedestroy($cert);