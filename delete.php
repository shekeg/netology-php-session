<?php
  require_once(__DIR__ . '/functions.php');

  if (isAdmin()) {
    $testId = $_GET['testID'];
    unlink(__DIR__ . "/tests/test-{$testId}.json");
    header('Location: test.php');
  } else {
    header('Location: test.php');
  }
?>