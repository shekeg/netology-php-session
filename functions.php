<?php
  session_start();

  function login($login, $password) {
    if (!empty($_COOKIE['attempt'])) {
      if ($_COOKIE['attempt'] > 6) {
        if ($_SESSION['captcha'] != $_POST['captcha'])
          return false;
      }
    }
    
    $user = getUser($login);
    if ($user && $user['password'] === $password) {
      $_SESSION['user'] = $user;
      $_SESSION['user']['isAdmin'] = true;
      return true;
    }
    elseif ($login && !$password) {
      $_SESSION['user']['login'] = $login;
      $_SESSION['user']['password'] = $password;
      $_SESSION['user']['isAdmin'] = false;
      return true;
    }
    return false;
  }

  function isAdmin() {
    return $_SESSION['user']['isAdmin'];
  }

  function isUnauthorized() {
    return empty($_SESSION['user']);
  }
  
  function getUser($login) {
    $fileData = file_get_contents(__DIR__ . '/login.json');
    $user = json_decode($fileData, true);
    if ($user['login'] === $login) {
      return $user;
    }
    return null;
  }
?>

