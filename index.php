<?php
  require_once(__DIR__ . '/functions.php');

  if (!empty($_COOKIE['blocked'])) {
    echo 'Подождите часок, мы вас заблокировали, а затем попробуйте снова';
    die();
  }

  $errors = [];
  if (!empty($_POST)) {
    if (login($_POST['login'], $_POST['password'])) {
      setcookie('attempt', 1);
      header('Location: test.php');
    } else {
      $attempt = (!empty($_COOKIE['attempt'])) ? $_COOKIE['attempt'] : 1;
      setcookie('attempt', ++$attempt, time()+30);
      if ($attempt >= 10) setcookie('blocked', true, time()+30);
      $errors[]='Неверные данные';
    }
  }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Вход в систему</title>
</head>
<body>
  <h1>Авторизация</h1>
  <ul>
    <?php foreach($errors as $error): ?>
      <li><?= $error ?></li>
    <?php endforeach; ?>
  </ul>

  <form action="index.php" method="POST">
    <label for="login">Логин</label>
    <input type="text" id="login" name="login" autocomplete="off">
    <label for="login">Пароль</label>
    <input type="password" id="password" name="password">
    <?php if (!empty($_COOKIE['attempt']) && $_COOKIE['attempt'] >= 6) : ?>
      <img src="captcha.php" alt="123">
      <input type="text" name="captcha" id="captcha">
    <?php endif ?>
    <input type="submit" value="Войти">
  </form>
</body>
</html>