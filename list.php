<?php
  require_once(__DIR__ . '/functions.php');

  if (isUnauthorized()) {
    http_response_code(403);
    die();
  }

  $testFiles = scandir(__DIR__ . '/tests');

  $testInfo = [];
  $i = 0;
  foreach(array_slice($testFiles, 2) as $testFile) {
    $testPath = 'tests/' . $testFile;
    $testContent = file_get_contents($testPath) or exit('Не удалось получить данные');
    $testInfo[$i]['name'] = json_decode($testContent, true)['name'];
    if ($testInfo[$i]['name'] === null) {exit('Ошибка декодирования JSON');}
    $testInfo[$i]['index'] = (int)substr($testFile, 5);
    $i++;
  }
?>

<h2>Перечень тестов</h2>
<?php foreach ($testInfo as $testQounter => $testInfoElem): ?>
  <a href="test.php?testID=<?php echo $testInfoElem['index'] ?>">
    <?php echo (!empty($testInfoElem['name']) ? $testInfoElem['name'] : 'Не удалось получить наименование теста') ?>
  </a>
  <?php if (isAdmin()) : ?>
    <span> | </span>
    <a href="delete.php?testID=<?php echo $testInfoElem['index'] ?>">Удалить тест</a><br>
  <?php else: ?>
    <br>
  <?php endif ?>
<?php endforeach ?>

