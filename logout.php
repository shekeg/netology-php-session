<?php
  require_once(__DIR__ . '/functions.php');
  
  unset($_COOKIE['attempt']);
  session_destroy();

  header('Location: index.php');
?>